<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ImportedDataTest extends TestCase
{
    
    public function test_import_category_data_from_external_source()
    {
        $this->json('GET', '/import-category')
        ->assertStatus(200)
        ->assertJson([
            "message" => "Category successfully imported."
        ]);
    }

    public function test_import_author_data_from_external_source()
    {
        $this->json('GET', '/import-author')
        ->assertStatus(200)
        ->assertJson([
            "message" => "Author successfully imported."
        ]);
    }

    public function test_import_customer_data_from_external_source()
    {
        $this->json('GET', '/import-customer')
        ->assertStatus(200)
        ->assertJson([
            "message" => "Customer successfully imported."
        ]);
    }

}
