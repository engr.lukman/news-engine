<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{

    public function testAuthorLogin()
    {
        $this->json('POST', 'api/author/login', [
                'email' => 'john.doe@example.com',
                'password' => 'secret_password',
            ])
            ->assertStatus(200)
            ->assertJson([
                "message" => "Author successfully logged."
            ]);
    }

    public function testCustomerLogin()
    {
        $this->json('POST', 'api/customer/login', [
                'email' => 'prothom.alo@example.com',
                'password' => 'secret_password',
            ])
            ->assertStatus(200)
            ->assertJson([
                "message" => "Customer successfully logged."
            ]);
    }

}
