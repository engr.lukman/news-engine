<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Author extends Authenticatable
{
    use HasFactory, HasApiTokens;

    public $timestamps = false;
    
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_professional',
    ];

    
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];
}
