<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Models\News;
use App\Models\ApiRequestCount;

class NewsController extends Controller
{
    public function createNews(Request $request)
    {
        if(auth()->guard('author-api')->check() && auth('author-api')->user()->token()->name != 'authorAuthToken') {
            return $this->apiResponse(Response::HTTP_UNAUTHORIZED, 'Your access token is invalid.');
        }

        $validator = Validator::make($request->input(), [
            'title' => 'required|max:255',
            'content' => 'required',
            'category_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->apiResponse(Response::HTTP_UNAUTHORIZED, $validator->errors());
        }

        $news = News::create([
            'category_id' => $request->category_id,
            'author_id' => auth('author-api')->user()->id,
            'title' => $request->title,
            'content' => $request->content,
        ]);

        $success['news'] = $news;

        return $this->apiResponse(Response::HTTP_CREATED , 'News successfully created.', $success);
    }

    public function getNews(Request $request)
    {
        if(auth()->guard('customer-api')->check() && auth('customer-api')->user()->token()->name != 'customerAuthToken') {
            return $this->apiResponse(Response::HTTP_UNAUTHORIZED, 'Your access token is invalid.');
        }

        $customer_id = auth('customer-api')->user()->id;
        $is_paid = auth('customer-api')->user()->is_paid;

        $apiRequestCount = ApiRequestCount::where(['customer_id' => $customer_id, 'date' => date('Y-m-d')])->first();

        if($apiRequestCount) {
            
            if( ($is_paid==0 && $apiRequestCount->count >= 10) || ($is_paid==1 && $apiRequestCount->count >= 100) ) {
                return $this->apiResponse(Response::HTTP_UNAUTHORIZED, 'Your daily api request access limit over.');
            }

            $apiRequestCount->count = $apiRequestCount->count+1;
        } else {
            $apiRequestCount = new ApiRequestCount; 
            $apiRequestCount->customer_id = $customer_id;
            $apiRequestCount->date = date('Y-m-d');   
            $apiRequestCount->count = 1;
        }
         
        $apiRequestCount->save();

        $whereParams = [];

        $category_id = $request->input('category_id');
        $title = $request->input('title');
        $created_at = $request->input('created_at');

        if (!empty($category_id))
        {
            $whereParams[] = ['news.category_id', '=', $category_id];
        }

        if (!empty($title))
        {
            $title = trim($title);
            $whereParams[] = [DB::raw("CONCAT(title,' ',content)"), "LIKE", "%$title%"];
        }

        if (!empty($created_at))
        {
            $whereParams[] = ['news.created_at', '>=', date('Y-m-d 00:00:00', strtotime($created_at))];
            $whereParams[] = ['news.created_at', '<=', date('Y-m-d 23:59:59', strtotime($created_at))];
        }

        $success['news'] = News::with(['category', 'author'])->where($whereParams)->limit(10)->orderBy('id', 'desc')->get();
        return $this->apiResponse(Response::HTTP_OK , 'News list.', $success);
    }
}
