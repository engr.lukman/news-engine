<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Author;
use App\Models\Customer;

class AuthController extends Controller
{ 
    public function authorLogin(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'email' => 'required|email',
            'password' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return $this->apiResponse(Response::HTTP_UNAUTHORIZED, $validator->errors());
        }

        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->guard('author')->attempt($data)) {
            $author = Author::find(auth()->guard('author')->user()->id);
            $success['token'] = $author->createToken('authorAuthToken')->accessToken; 
            $success['author'] = $author;

            return $this->apiResponse(Response::HTTP_OK , 'Author successfully logged.', $success);
        } else {
            return $this->apiResponse(Response::HTTP_UNAUTHORIZED, 'Author login credential not matched.');
        }
    } 

    public function authorLogout (Request $request) 
    {
        $token = auth('author-api')->user()->token();        
        $token->revoke();

        return $this->apiResponse(Response::HTTP_OK , 'You have been successfully logged out!', );
    }

    public function customerLogin(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'email' => 'required|email',
            'password' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return $this->apiResponse(Response::HTTP_UNAUTHORIZED, $validator->errors());
        }

        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->guard('customer')->attempt($data)) {
            $customer = Customer::find(auth()->guard('customer')->user()->id);
            $success['token'] = $customer->createToken('customerAuthToken')->accessToken; 
            $success['customer'] = $customer;

            return $this->apiResponse(Response::HTTP_OK , 'Customer successfully logged.', $success);
        } else {
            return $this->apiResponse(Response::HTTP_UNAUTHORIZED, 'Customer login credential not matched.');
        }
    }   

    public function customerLogout (Request $request) 
    {
        $token = auth('customer-api')->user()->token();        
        $token->revoke();

        return $this->apiResponse(Response::HTTP_OK , 'You have been successfully logged out!', );
    }

}
