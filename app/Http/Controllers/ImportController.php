<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class ImportController extends Controller
{

    public function __construct(        
        private $timeout = 10,
        private $categoryUrl = 'https://gist.githubusercontent.com/nscreed/aad17fa33742a0d412ac657891d0a057/raw/7926809296d10fbe69439c9312cac5e1b50f0816/categories.json',
        private $authorUrl = 'https://gist.githubusercontent.com/nscreed/aad17fa33742a0d412ac657891d0a057/raw/7926809296d10fbe69439c9312cac5e1b50f0816/authors.json',
        private $customerUrl = 'https://gist.githubusercontent.com/nscreed/aad17fa33742a0d412ac657891d0a057/raw/7926809296d10fbe69439c9312cac5e1b50f0816/customers.json'
    ) {}

    public function importCategory()
    {

        try {
            $response = Http::acceptJson()->timeout($this->timeout)->get($this->categoryUrl);

            if($response && $response->status()==200 && $response->body()) {

                $items = json_decode($response->body());
                $insertedData = [];

                foreach($items as $item) {
                    if (DB::table('categories')->where('title', $item->title)->doesntExist()) {
                        $insertedData[] = [
                            'title' => $item->title
                        ];
                    }                        
                }

                if(!empty($insertedData)) {
                    DB::table('categories')->insert($insertedData);
                }
                
            }

            return response()->json(['message' => 'Category successfully imported.']);

        } catch (\Exception $e) {
            return "Error: ".$e;
        }

    }
    
    public function importAuthor()
    {

        try {
            $response = Http::acceptJson()->timeout($this->timeout)->get($this->authorUrl);

            if($response && $response->status()==200 && $response->body()) {

                $items = json_decode($response->body());
                $insertedData = [];

                foreach($items as $item) {
                    if (DB::table('authors')->where('email', $item->email)->doesntExist()) {
                        $insertedData[] = [
                            'name' => $item->name,
                            'email' => $item->email,
                            'password' => bcrypt($item->password),
                            'is_professional' => $item->is_professional,
                        ];
                    }                        
                }

                if(!empty($insertedData)) {
                    DB::table('authors')->insert($insertedData);
                }
                
            }

            return response()->json(['message' => 'Author successfully imported.']);

        } catch (\Exception $e) {
            return "Error: ".$e;
        }

    }

    public function importCustomer()
    {

        try {
            $response = Http::acceptJson()->timeout($this->timeout)->get($this->customerUrl);

            if($response && $response->status()==200 && $response->body()) {

                $items = json_decode($response->body());
                $insertedData = [];

                foreach($items as $item) {
                    if (DB::table('customers')->where('email', $item->email)->doesntExist()) {
                        $insertedData[] = [
                            'name' => $item->name,
                            'email' => $item->email,
                            'password' => bcrypt($item->password),
                            'is_paid' => $item->is_paid,
                        ];
                    }                        
                }

                if(!empty($insertedData)) {
                    DB::table('customers')->insert($insertedData);
                }
                
            }

            return response()->json(['message' => 'Customer successfully imported.']);

        } catch (\Exception $e) {
            return "Error: ".$e;
        }

    }

}
