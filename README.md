# News Engine
-----------------------
### _Used framework, libraries and tools_
 - PHP 8.x
 - Laravel 9.x
 - Laravel passport multi authentication for API 
 - Postman
 - Composer
### _Installation_
 - Clone source code from gitlab ```https://gitlab.com/engr.lukman/news-engine.git```
 - Create database name is ```news_engine```
 - Update composer ```composer update```
 - Run migration ```php artisan migrate```
 - Run project ```php artisan serve```
 ### _Import (Category, Author and Customer) data from external source_
 - **Category Import:**  Copy url and paste in browser address bar ```http://localhost:8000/import-category```
 - **Author Import:**  Copy url and paste in browser address bar ```http://localhost:8000/import-author```
 - **Customer Import:**  Copy url and paste in browser address bar ```http://localhost:8000/import-customer```

## API
- Import postman collection from folder ```docs/postman_collection.json```
- ***Author Login*** ```http://localhost:8000/api/author/login```
 ![Author Login](https://gitlab.com/engr.lukman/news-engine/-/raw/main/docs/author-login.png)
- ***Customer Login*** ```http://localhost:8000/api/customer/login```
![Author Login](https://gitlab.com/engr.lukman/news-engine/-/raw/main/docs/customer-login.png)
- ***Crerate News*** ```http://localhost:8000/api/news/create```
![Author Login](https://gitlab.com/engr.lukman/news-engine/-/raw/main/docs/create-news.png)
- ***News List*** ```http://localhost:8000/api/news```
![Author Login](https://gitlab.com/engr.lukman/news-engine/-/raw/main/docs/news-list.png)

## Unit Testing
```shell
php artisan test
```
### License
**MIT Free Software!**

# Project's Requirements (Suppose you are an API provider of a news engine.)
-----------------------

#### Sample database design.

###### Feel free to add/modify tables, columns when necessary.
-----------------------
```yaml
Category
    - id
    - title (unique)

Author
    - id
    - name
    - email (unique)
    - password
    - is_professional (boolean)

Customer
    - id
    - name
    - email (unique)
    - password
    - is_paid (boolean)

News
    - id
    - category_id
    - author_id
    - title (string)
    - content (text)
    - created_at
    - updated_at
```
-------------------------

#### Business requirements:

- Per day - a free customer can call 10 API requests.
- Per day - a premium/paid customer can call 100 API requests.
- Per call - a customer can retrieve a maximum of 10 news.
- All the news of a professional author is paid and only accessible by the premium customer.

#### Consider the following points when developing your API:

- Author, members, and categories should be imported to your database from an external source.
- API Security.
- It can be developed using a Framework.
- Clean and readable code.

#### Develop following APIs:

- Account Login.
    - Sample Payload
    ```json
    {
        "email": "bd.news@example.com",
        "password": "secret_password"
    }
    ```

- Create news as an author.
    - Sample Payload:
    ```json
    {
      "title": "A simple news title",
      "content": "The actual news; it should be a long text.",
      "category_id": 1
    }
    ```
        
- Fetch news as a customer
    - Consider filter when possible
        - Only Paid News (for premium customers)
        - By Categories
        - Filter by title
        - Created Date

#### Other items:
- Develop a mechanism to import Categories, Authors and Customers from the external sources; the data (NOT SCHEMA) can be updated at any time.

#### Resources:
- Consider the following external sources for your initial development:
    - Authors: 
        - https://gist.githubusercontent.com/nscreed/aad17fa33742a0d412ac657891d0a057/raw/7926809296d10fbe69439c9312cac5e1b50f0816/authors.json
    - Categories: 
        - https://gist.githubusercontent.com/nscreed/aad17fa33742a0d412ac657891d0a057/raw/7926809296d10fbe69439c9312cac5e1b50f0816/categories.json
    - Customers: 
        - https://gist.githubusercontent.com/nscreed/aad17fa33742a0d412ac657891d0a057/raw/7926809296d10fbe69439c9312cac5e1b50f0816/customers.json

#### Note:
- Admin Panel or web interface IS NOT required.
- Push your code to git (preferably GitHub or bitbucket) from the beginning and try to commit often not all code at once. And when finish gives me access to your codebase.
- Provide necessary documentations.