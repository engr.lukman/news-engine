<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\NewsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(AuthController::class)->group(function() {
    Route::post('author/login', 'authorLogin');    
    Route::post('author/logout', 'authorLogout');
    Route::post('customer/login', 'customerLogin');
    Route::post('customer/logout', 'customerLogout');
});

Route::controller(NewsController::class)->group(function() {
    Route::post('news/create', 'createNews')->middleware(['auth:author-api']);
    Route::post('news', 'getNews')->middleware(['auth:customer-api']);
});