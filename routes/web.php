<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ImportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(ImportController::class)->group(function() {
    Route::get('import-category', 'importCategory')->name('import.category');
    Route::get('import-author', 'importAuthor')->name('import.author');
    Route::get('import-customer', 'importCustomer')->name('import.customer');
});
